package backend;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Scheduler implements Runnable{
  private ArrayList<Client> clientsToAdd;
  private ArrayList<ClientQueue> queues;
  private int simulationTime;
  private int timeLimit;
  private boolean running;
  private String log;
  
  public Scheduler(int minArrivalTime, int maxArrivalTime, int minServieTime, int maxServiceTime, int nrOfQueues, int timeLimit) {
    Random generator = new Random();
    this.log = "";
    this.clientsToAdd = new ArrayList<Client>();
    this.queues = new ArrayList<ClientQueue>();
    this.timeLimit = timeLimit;
    this.simulationTime = 0;
    for(int i = 0; i < 20; i++) {
      Client client = new Client(minArrivalTime + generator.nextInt(maxArrivalTime-minArrivalTime),  
                                 minServieTime + generator.nextInt(maxServiceTime-minServieTime), i);
      clientsToAdd.add(client);
    }
    for(int i = 0; i < nrOfQueues; i++) {
      queues.add(new ClientQueue());
    }
  }
  
  public void run() {
    this.running = true;
    ExecutorService executor = Executors.newFixedThreadPool(5);
    for(ClientQueue cq : queues) {
      executor.execute(cq);
    }
    while(simulationTime < timeLimit) {
      int i = 0;
      for(ClientQueue cq: queues) {
        if(cq.isFirstClientDone()) {
          log += cq.removeClientFromList() + " " + (++i) + " at time " + simulationTime + "\n";
        }
      }
      int queueIndex = getBestQueueIndex();
      while(addClientToQueue(queueIndex)) {
        queueIndex = getBestQueueIndex();
      }
      simulationTime++;
      ClientQueue.increaseSimulationTime();
      try {Thread.sleep(1000);}
      catch (InterruptedException e) {}
    }
    for(ClientQueue cq : queues) {
      cq.setRunning(false);
    }
    executor.shutdown();
    while(!executor.isTerminated()) {}
    this.running = false;
  }
  
  public boolean getRunning() {
    return this.running;
  }
  
  public String getLog() {
    return this.log;
  }
  
  public int getBestQueueIndex() {
    int mini = queues.get(0).getWaitingTime();
    int miniIndex = 0;
    for(int i = 1; i < queues.size(); i++) {
      if(queues.get(i).getWaitingTime() < mini) {
        mini = queues.get(i).getWaitingTime();
        miniIndex = i;
      }
    }
    return miniIndex;
  }
  
  public boolean addClientToQueue(int queueIndex) {
    for(Client c : clientsToAdd) {
      if(c.getArrivalTime() <= simulationTime) {
        ClientQueue queue = queues.get(queueIndex);
        queue.addClientToList(c);
        clientsToAdd.remove(c);
        queues.set(queueIndex, queue);
        log += "Client " + c.getClientID() +" has been addded to queue " + (queueIndex+1) + " at time " + simulationTime + "\n";
        return true;
      }
    }
    
    return false;
  }
  
  public String toString() {
    String rez = "";
    int i = 1;
    for(ClientQueue cq : queues) {
      rez += i + " Queue is " + cq.toString();
      i++;
    }
    return rez;
  }
}
