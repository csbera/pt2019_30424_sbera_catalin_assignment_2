package backend;

import java.util.ArrayList;

public class ClientQueue implements Runnable{

  private ArrayList<Client> clients;
  private boolean running;
  private int waitingTime;
  private int servedClients;
  private float avgWaitTime;
  private float avgServiceTime;
  private int freeTime;
  private static int simultaionTime = 0;
  private int peakTime;
  private int peakClients;
  
  
  public ClientQueue() {
    this.peakClients = 0;
    this.clients = new ArrayList<Client>();
    this.servedClients = 0;
    this.running = true;
  }
  
  public int getWaitingTime() {
    return this.waitingTime;
  }
  
  public void setRunning(boolean running) {
    this.running = running;
  }
  
  public boolean isFirstClientDone() {
    if(!clients.isEmpty()) {
      return clients.get(0).getArrivalTime() + clients.get(0).getServiceTime() <= simultaionTime;
    }
    return false;
  }

  public void addClientToList(Client client) {
    clients.add(client);
    waitingTime += client.getServiceTime();
  }
  
  public String removeClientFromList() {
    if(!clients.isEmpty()) {
      Client client = clients.get(0);
      if(clients.size() >= 2) {
      Client nextClient = clients.get(1);
        avgWaitTime += simultaionTime - nextClient.getServiceTime();
        nextClient.setArrivalTime(simultaionTime);
        clients.set(1, nextClient);
      }
      clients.remove(0);
      avgServiceTime += client.getServiceTime();
      servedClients++;
      waitingTime -= client.getServiceTime();
      return "Client " + client.getClientID() + " has been removed from queue";
    }
    return "";
  }
  
  public void run() {
    while(running) {
     if(clients != null && clients.size() > peakClients) {
       peakClients = clients.size();
       peakTime = simultaionTime;
     }
     if(clients.isEmpty()) freeTime++;
     //System.out.println("    " + this.toString());
     try {
      Thread.sleep(1000);
      }
      catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
    //System.out.println("    " + Thread.currentThread().getName() + " ended");
  }

  @Override
  public String toString() {
    String rez = "";
    if(!this.clients.isEmpty()) {
      rez += "procesing: ";
      for(Client c: clients)
        rez += c.getClientID() + " ";
    }
    else {
      rez += "free: ";
    }
    rez += "\n   Avg wait time: " + avgWaitTime/servedClients + " Avg service time: " + avgServiceTime/servedClients + "\n   Empty queue time: " + freeTime + " "; 
    rez += "Peak Time: " + peakTime + " Peak Clients: " + peakClients + "\n";
    return rez;
  }

  public static void increaseSimulationTime() {
    ClientQueue.simultaionTime++;
  }
}
