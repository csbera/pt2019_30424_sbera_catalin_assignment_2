package frontend;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.Border;

public class InitialView {
  public InitialView() {
    final JFrame f= new JFrame("ThreadApp");
    f.setLayout(new GridLayout(4, 1));
    Border border = BorderFactory.createLineBorder(Color.black);
    
    JPanel arrival = new JPanel();
    arrival.setLayout(new GridLayout(1, 3));
    JLabel arrivalInterval = new JLabel("Arrival Interval");
    final JTextArea arrivalIntervalMin = new JTextArea("3");
    final JTextArea arrivalIntervalMax = new JTextArea("20");
    arrival.add(arrivalInterval);
    arrival.add(arrivalIntervalMin);
    arrival.add(arrivalIntervalMax);
    f.add(arrival);
    
    JPanel service = new JPanel();
    service.setLayout(new GridLayout(1, 3));
    JLabel serviceInterval = new JLabel("Service Interval");
    final JTextArea serviceIntervalMin = new JTextArea("4");
    final JTextArea serviceIntervalMax = new JTextArea("10");
    service.add(serviceInterval);
    service.add(serviceIntervalMin);
    service.add(serviceIntervalMax);
    f.add(service);
    
    JPanel last = new JPanel();
    last.setLayout(new GridLayout(1, 4));
    JLabel queueNumber = new JLabel("Number of Queues");
    JLabel simulationDuration = new JLabel("Duration of the simulation");
    final JTextArea queueNumberArea = new JTextArea("3");
    final JTextArea simulationDurationArea = new JTextArea("40");
    last.add(queueNumber);
    last.add(queueNumberArea);
    last.add(simulationDuration);
    last.add(simulationDurationArea);
    f.add(last);
    
    JButton start = new JButton("Start Simulation");
    f.add(start);
    
    start.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) {
        try {
        int minArrival = Integer.parseInt(arrivalIntervalMin.getText());
        int maxArrival = Integer.parseInt(arrivalIntervalMax.getText());
        int minService = Integer.parseInt(serviceIntervalMin.getText());
        int maxService = Integer.parseInt(serviceIntervalMax.getText());
        int nrQueues = Integer.parseInt(queueNumberArea.getText());
        int simulationTime = Integer.parseInt(simulationDurationArea.getText());
        f.dispose();
        MainView mv = new MainView(minArrival,maxArrival,minService,maxService,nrQueues,simulationTime);
        }catch(NumberFormatException er) {
          f.setTitle("Invalid Input");
        }
    }} );
    
    
    f.setSize(600,600);       
    f.setVisible(true);
    
  }
  public static void main(String args[]){  
    InitialView b = new InitialView();  
  }  
}
