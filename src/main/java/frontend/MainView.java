package frontend;

import backend.*;

import java.awt.Frame;
import java.awt.GridLayout;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.*;
import javax.swing.SwingWorker;

public class MainView extends Frame {

  private JTextArea queues;
  private JTextArea console;
  
  public MainView (int minArrivalTime, int maxArrivalTime, int minServieTime, int maxServiceTime, int nrOfQueues, int timeLimit) {  

     JFrame f= new JFrame("ThreadApp");  
     
     GridLayout layout = new GridLayout(2, 1);
     layout.setVgap(10);
     f.setLayout(layout);
     queues = new JTextArea("");
     JScrollPane queueScrollPane = new JScrollPane(queues);
     queues.setFont(queues.getFont().deriveFont(20f));
     console = new JTextArea("");
     JScrollPane consoleScrollPane = new JScrollPane(console);
     f.add(queueScrollPane);
     f.add(consoleScrollPane);
     
     Scheduler scheduler = new Scheduler(minArrivalTime, maxArrivalTime, minServieTime, maxServiceTime, nrOfQueues, timeLimit);
     SwingWorker worker1 = createWorkerConsole(scheduler,console);
     SwingWorker worker2 = createWorkerQueue(scheduler,queues);
     ExecutorService executor = Executors.newFixedThreadPool(3);
     executor.execute(worker1);
     executor.execute(worker2);
     executor.execute(scheduler);
     queues.setText("asd");
     queueScrollPane.updateUI();
     
     f.setSize(600,600);       
     f.setVisible(true);
     
 }

  public SwingWorker createWorkerConsole(final Scheduler scheduler, final JTextArea resultArea) {
    return new SwingWorker<Void, String>() {
      @Override
      protected Void doInBackground() throws Exception {
        //publish("Start");
        waitFor(500);
        while(scheduler.getRunning()) {
          publish(scheduler.getLog());
          waitFor(500);
        }
        publish(scheduler.getLog() + "Simulation Complete");
        return null;
      }
      
      @Override
      protected void process(List<String> chunks) {
        for(String s: chunks) {
          resultArea.setText(s);
        }
      }
    };
  }
  
  public SwingWorker createWorkerQueue(final Scheduler scheduler, final JTextArea resultArea) {
    return new SwingWorker<Void, String>() {
      @Override
      protected Void doInBackground() throws Exception {
        publish("Starting");
        waitFor(500);
        while(scheduler.getRunning()) {
          publish(scheduler.toString());
          waitFor(500);
        }
        return null;
      }
      
      @Override
      protected void process(List<String> chunks) {
        for(String s: chunks) {
          resultArea.setText(s);
        }
      }
    };
  }
  
  private void waitFor (int milliseconds) {
    try {
        Thread.sleep(milliseconds);
    }
    catch (Exception e) {
        e.printStackTrace();
    }
}
}
